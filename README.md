## Warning

I simply assume you have a luks encrypted partition that contains everything
that is needed to start your system. If you need help with modification for
your specific setup just let me know.

Example:

```
sda           # luks encrypted harddisk (put UUID in LUKS_UUID)
└─root        # lv group
  ├─lvm-root  # logical volume containing initramfs and kernel (put UUID in ROOT_UUID)
  └─lvm-home  # some other volume
sdb           # another harddisk
└─sdb1        # unencrypted partition with linux (put UUID in CLEAN_UUID)
```

## Build

Make sure to have all dependencies.

On Archlinux run: `pacman -S base-devel bdf-unifont && pacaur -Ss ckbcomp`

Build grub2:
```
git clone git://git.savannah.gnu.org/grub.git
cd grub
./autogen.sh
./configure --with-platform=coreboot
make
```

Copy `config.local.example ` to `config.local` and change it to your liking.
`GRUB_DIR` has to point to the grub directory you just cloned.

When you now run `make` the resulting `build/grub2.elf` can be used as a
coreboot payload.


## What may be done in the future?

- be more generic for other layouts and options (more templates)


## References

- https://github.com/bibanon/Coreboot-ThinkPads/wiki/Compiling-GRUB2-for-Coreboot
