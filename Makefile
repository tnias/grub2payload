
include config.local

BUILD_DIR:=build

MODULES:=$(shell bash ./get_modules.sh | head -1)
INSTALL_MODULES:=$(shell bash ./get_modules.sh | tail -1)

default: ${BUILD_DIR} ${BUILD_DIR}/grub2.elf

config.local: config.local.example
	test -f config.local || cp -v config.local.example config.local
	# in case it already existed we change the date
	touch config.local

${BUILD_DIR}:
	@mkdir -pv "${BUILD_DIR}"

${BUILD_DIR}/grub.cfg: ${GRUB_DIR} get_config.sh config.local config.common
	echo "creating config"
	bash ./get_config.sh > "${BUILD_DIR}/grub.cfg"

${BUILD_DIR}/${GRUB_LAYOUT}.gkb: ${GRUB_DIR}
	ckbcomp "${GRUB_LAYOUT}" | \
		"${GRUB_DIR}/grub-mklayout" -o "${BUILD_DIR}/${GRUB_LAYOUT}.gkb"

${BUILD_DIR}/grub2.elf: ${GRUB_DIR} ${BUILD_DIR}/grub.cfg ${GRUB_DIR}/euro.pf2 ${BUILD_DIR}/${GRUB_LAYOUT}.gkb
	"${GRUB_DIR}/grub-mkstandalone" \
		--grub-mkimage="${GRUB_DIR}/grub-mkimage" \
		-O i386-coreboot \
		-o "${BUILD_DIR}/grub2.elf" \
		--modules="${MODULES}" \
		--install-modules="${INSTALL_MODULES}" \
		--fonts=euro \
		--themes= \
		--locales= \
		-d "${GRUB_DIR}/grub-core" \
		"/boot/grub/grub.cfg=${BUILD_DIR}/grub.cfg" \
		"/boot/grub/layouts/${GRUB_LAYOUT}.gkb=${BUILD_DIR}/${GRUB_LAYOUT}.gkb" \
		"/boot/grub/fonts/euro.pf2=${GRUB_DIR}/euro.pf2" \
		"/boot/grub/splash.png=./splash.png" # uncomment to get splash

clean:
	rm -rv "${BUILD_DIR}"
